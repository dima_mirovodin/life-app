# Life Game #

This is "Life" game implementation.

![Screen Shot 2014-11-02 at 21.47.59.png](https://bitbucket.org/repo/Bd84nX/images/3785712126-Screen%20Shot%202014-11-02%20at%2021.47.59.png)

### System requirements ###

* JDK 1.7 (http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
* Maven 3 (http://maven.apache.org/download.cgi)

### How to run? ###

* Download source:
```
#!bash
git clone https://dima_mirovodin@bitbucket.org/dima_mirovodin/life-app.git
```
* Go to "life-app" folder:
```
#!bash
cd life-app
```
* Build project:

```
#!bash
mvn clean install
```
* Execute project:

```
#!bash
mvn exec:java -Dexec.mainClass="com.life.app.AppBootstrap"
```

* That's it! Have fun :)

P.S. The program was tested on OS X. If you have any problem in another OS, please email me.