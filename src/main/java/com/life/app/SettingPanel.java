package com.life.app;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class SettingPanel extends JPanel {

    public interface Handler {
        void onStartGame(int width, int height);
    }

    private Handler handler;
    private JFormattedTextField fieldWidth;
    private JFormattedTextField fieldHeight;
    private JButton startGameButton;

    public SettingPanel() {

        JPanel panel = new JPanel(new MigLayout("fill"));

        panel.add(new JLabel("<html><h1>Life</h1></html>"), "span, grow, wrap");

        panel.add(new JLabel("Field width: "));
        fieldWidth = createTextFiled();
        panel.add(fieldWidth, "w 50!, wrap");

        panel.add(new JLabel("Field height: "));
        fieldHeight = createTextFiled();
        panel.add(fieldHeight, "w 50!, wrap");

        startGameButton = new JButton("Start");
        panel.add(startGameButton, "span, grow, wrap");
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (handler != null) {
                    int width = (Integer)fieldWidth.getValue();
                    int height = (Integer)fieldHeight.getValue();

                    handler.onStartGame(width, height);
                }
            }
        });

        add(panel);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private JFormattedTextField createTextFiled() {

        NumberFormat format = DecimalFormat.getInstance();
        format.setGroupingUsed(false);

        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(Setting.FIELD_MIN_SIZE);
        formatter.setMaximum(Setting.FIELD_MAX_SIZE);
        formatter.setCommitsOnValidEdit(true);
        formatter.setAllowsInvalid(false);

        JFormattedTextField textField = new JFormattedTextField(formatter);
        textField.setValue(Setting.FIELD_MIN_SIZE);

        return  textField;
    }
}
