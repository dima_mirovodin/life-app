package com.life.app;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.*;

public class GridController {

    public enum CellState {
        EMPTY,
        EXISTS,
        OUT_OF_BOUNDS,
    }

    int dx;
    int dy;

    private JScrollBar verticalScrollBar;
    private JScrollBar horizontalScrollBar;
    private JPanel gridPanel;
    private GridData gridData;
    private JLabel statusLabel;

    public void initialize(JScrollBar verticalScrollBar, JScrollBar horizontalScrollBar, JPanel gridPanel, JLabel statLabel, GridData gridData) {
        this.verticalScrollBar = verticalScrollBar;
        this.horizontalScrollBar = horizontalScrollBar;
        this.gridPanel = gridPanel;
        this.gridData = gridData;
        this.statusLabel = statLabel;

        setup();
    }

    private void setup() {
        dx = 0;
        dy = 0;

        AdjustmentListener scrollBarListener =  new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                processScrollBarChange(e);
            }
        };

        verticalScrollBar.setMinimum(0);
        verticalScrollBar.addAdjustmentListener(scrollBarListener);
        verticalScrollBar.setOrientation(JScrollBar.VERTICAL);

        horizontalScrollBar.setMinimum(0);
        horizontalScrollBar.addAdjustmentListener(scrollBarListener);
        horizontalScrollBar.setOrientation(JScrollBar.HORIZONTAL);

        gridPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                processGridResize();
            }
        });

        gridPanel.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                processGridClick(e);
            }
        });

        gridData.setHandler(new GridData.Handler() {
            @Override
            public void onUpdated() {
                processGridUpdate();
            }
        });

        updateViewSize();
        updateStatistic();
    }

    public Rectangle getViewPort() {
        int viewWidth = (gridPanel.getWidth() /Setting.GRID_DOT_SIZE) - 1;
        int viewHeight = (gridPanel.getHeight() /Setting.GRID_DOT_SIZE) - 1;

        return new Rectangle(dx, dy, viewWidth, viewHeight);
    }


    private void updateViewSize() {
        Rectangle viewPort = getViewPort();

        boolean verticalEnable = (gridData.getHeight() - viewPort.height) >= 0;
        verticalScrollBar.setEnabled(verticalEnable);
        if (verticalEnable) {
            verticalScrollBar.setMaximum(gridData.getHeight() - viewPort.height);
            verticalScrollBar.setVisibleAmount(dy);

        }

        boolean horizontalEnable = (gridData.getWidth() - viewPort.width) >= 0;
        horizontalScrollBar.setEnabled(horizontalEnable);
        if (horizontalEnable) {
            horizontalScrollBar.setMaximum(gridData.getWidth() - viewPort.width);
            horizontalScrollBar.setVisibleAmount(dx);
        }
    }

    private void processGridResize() {
        updateViewSize();
    }

    private void processScrollBarChange(AdjustmentEvent e) {
        if (e.getSource() == verticalScrollBar) {
            if (dy != verticalScrollBar.getValue()) {
                dy = verticalScrollBar.getValue();
                processGridUpdate();
            }
        } else if (e.getSource() == horizontalScrollBar) {
            if (dx != horizontalScrollBar.getValue()) {
                dx = horizontalScrollBar.getValue();
                processGridUpdate();
            }
        }
    }

    private void processGridUpdate() {
        updateStatistic();
        gridPanel.repaint();
    }

    private void updateStatistic() {
        String style = gridData.getDelta() >= 0 ? "style='color:green'" : "style='color:red'";

        statusLabel.setText("<html>Steps: <b>" + gridData.getStepCount() + "</b> <br> Alive : <b>" + gridData.getAlive() + "</b><br> Delta : <b " + style + ">" + gridData.getDelta() + "</b></html>");
    }

    private void processGridClick(MouseEvent e) {

        int cellX = dx + (e.getX() / Setting.GRID_DOT_SIZE);
        int cellY = dy + (e.getY() / Setting.GRID_DOT_SIZE);

        if (gridData.isValidPosition(cellX, cellY)) {
            boolean value = gridData.getValue(cellX, cellY);
            gridData.setValue(cellX, cellY, !value);
        }
    }

    public CellState getCellValue(int x, int y) {
        int cellX = dx + x;
        int cellY = dy + y;

        if (gridData.isValidPosition(cellX, cellY)) {
            return gridData.getValue(cellX, cellY) ? CellState.EXISTS : CellState.EMPTY;
        } else {
            return CellState.OUT_OF_BOUNDS;
        }
    }
}
