package com.life.app;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

public class MainFrame extends JFrame {
    private GridData gridData;
    private GridPanel gridPanel;
    private GridController controller;
    private JPanel mainPanel;
    private JButton continueButton;
    private JButton stepButton;
    private JScrollBar gameSpeedScrollBar;
    private Timer gameTimer;
    private JLabel statusLabel;
    private Storage storage;

    final static String PAGE_GAME = "game page card";
    final static String PAGE_SETTING = "setting page card";

    public MainFrame() {
        gridPanel = new GridPanel();
        gameTimer = new Timer(Setting.GAME_TIMER_MAX, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                processGameStep();
            }
        } );
        gameTimer.stop();

        storage = new Storage();
        setup();
        layoutComponents();
    }

    private void setup() {
        setTitle("Life");
        setSize(600, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void layoutComponents() {
        mainPanel = new JPanel(new CardLayout());
        add(mainPanel);

        mainPanel.add(createGamePage(), PAGE_GAME);
        mainPanel.add(createSettingPage(), PAGE_SETTING);

        switchPage(PAGE_SETTING);
    }

    private JPanel createSettingPage() {
        JPanel container = new JPanel();

        SettingPanel settingPanel = new SettingPanel();
        settingPanel.setHandler(new SettingPanel.Handler() {
            @Override
            public void onStartGame(int width, int height) {
                initGame(width, height);
            }
        });
        container.add(settingPanel);

        return container;
    }

    private JPanel createGamePage() {
        JPanel panel = new JPanel(new MigLayout("fill, ins 0, gap 0"));

        panel.add(gridPanel, "grow, push");

        JPanel controlPanel = new JPanel(new MigLayout("fill, flowy, ay top"));

        continueButton = new JButton("Start");
        controlPanel.add(continueButton, "grow");
        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                processContinueButtonClick();
            }
        });

        stepButton = new JButton("Step");
        controlPanel.add(stepButton, "grow");
        stepButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                processStepButtonClick();
            }
        });

        JButton randomButton = new JButton("Random");
        controlPanel.add(randomButton, "grow");
        randomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gridData.random();
            }
        });

        JButton clear = new JButton("Clear");
        controlPanel.add(clear, "grow");
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gridData.clear();
            }
        });

        JButton save = new JButton("Save");
        controlPanel.add(save, "grow");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                storage.saveToFile(gridData.getMap(), gridData.getWidth(), gridData.getHeight());
            }
        });

        controlPanel.add(new JLabel("Speed"), "grow");

        gameSpeedScrollBar = new JScrollBar();
        gameSpeedScrollBar.setMinimum(Setting.GAME_TIMER_MIN);
        gameSpeedScrollBar.setMaximum(Setting.GAME_TIMER_MAX);
        gameSpeedScrollBar.setValue(Setting.GAME_TIMER_MAX / 2);
        gameSpeedScrollBar.setVisibleAmount(Setting.GAME_TIMER_MIN);
        gameSpeedScrollBar.setOrientation(JScrollBar.HORIZONTAL);
        gameSpeedScrollBar.addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                gameTimer.setDelay(gameSpeedScrollBar.getValue());
            }
        });

        controlPanel.add(gameSpeedScrollBar, "grow");

        statusLabel = new JLabel();
        controlPanel.add(statusLabel, "grow");

        panel.add(controlPanel, "w 200!, growx, ay top, wrap");

        return panel;
    }

    private void processStepButtonClick() {
        processGameStep();
    }

    private void processContinueButtonClick() {
        if (gameTimer.isRunning()) {
            continueButton.setText("Continue");
            gameTimer.stop();
            stepButton.setEnabled(true);
        } else {
            continueButton.setText("Pause");
            stepButton.setEnabled(false);
            gameTimer.start();
        }
    }

    private void processGameStep() {
        gridData.nextStep();
    }

    private void switchPage(String page) {
        CardLayout cl = (CardLayout)(mainPanel.getLayout());
        cl.show(mainPanel, page);
    }

    private void initGame(int fieldWidth, int fieldHeight) {
        gridData = new GridData(fieldWidth, fieldHeight);
        controller = new GridController();

        gridPanel.setController(controller);

        controller.initialize(gridPanel.getVerticalScrollBar(), gridPanel.getHorizontalScrollBar(),
                gridPanel.getGridPanel(), statusLabel, gridData);

        switchPage(PAGE_GAME);
    }

}
