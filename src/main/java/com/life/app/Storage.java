package com.life.app;

import javax.swing.*;
import java.io.*;
import java.util.Calendar;

public class Storage {

    public void saveToFile(final boolean map[][], final int width, final int height) {

        SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {

            protected void done() {
                try {
                    String fileName = get();

                    System.out.println("File was created : " + fileName);

                } catch (Exception e) {
                    System.out.println("Error create file : " + e);
                }
            }

            @Override
            protected String doInBackground() throws Exception {

                String fileName = null;
                try {

                    File dir = new File(getExportFolder());
                    if (!dir.exists()) {
                        if (!dir.mkdir()) return null;
                    }

                    Calendar c = Calendar.getInstance();

                    fileName = dir.getPath() + "/" + c.getTimeInMillis() + ".txt";

                    FileOutputStream fos = new FileOutputStream(fileName);
                    DataOutputStream dos = new DataOutputStream(fos);

                    dos.writeInt(width);
                    dos.writeInt(height);

                    for (int i = 0; i <= width; i++) {
                        for (int j=0; j<= height; j++) {
                            dos.writeBoolean(map[i][j]);
                        }
                    }

                    fos.flush();
                    fos.close();

                } catch (Exception e) {
                    return null;
                }
                return fileName;
            }
        };

        worker.execute();
    }

    private static String getExportFolder() {
        return System.getProperty("user.dir") + "/storage/";
    }
}
