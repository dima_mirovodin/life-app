package com.life.app;

import java.util.Random;

public class GridData {

    public interface Handler {
        void onUpdated();
    }

    private boolean [][] map;
    private boolean [][] newMap;

    private int width;
    private int height;
    private int stepCount;
    private int alive;
    private int delta;
    private Handler handler;

    public GridData(int width, int height) {
        this.width = width;
        this.height = height;
        map = new boolean[width+1][height+1];
        newMap = new boolean[width+1][height+1];
    }

    public void nextStep() {
        int oldAlive = alive;
        alive = 0;

        // based on http://rosettacode.org/wiki/Conway's_Game_of_Life#C.23, C# version
        // plus my bugfix

        for (int i = 0; i <= width; i++) {
            for (int j = 0; j <= height; j++) {

                newMap[i][j] = false;

                int neighbors = 0;

                neighbors += getValue(i, j+1) ? 1: 0;
                neighbors += getValue(i, j-1) ? 1: 0;
                neighbors += getValue(i+1, j) ? 1: 0;
                neighbors += getValue(i-1, j) ? 1: 0;
                neighbors += getValue(i+1, j+1) ? 1: 0;
                neighbors += getValue(i-1, j-1) ? 1: 0;
                neighbors += getValue(i+1, j-1) ? 1: 0;
                neighbors += getValue(i-1, j+1) ? 1: 0;

                boolean newValue = (getValue(i, j) && neighbors == 2) || neighbors == 3;

                newMap[i][j] = newValue;

                alive += newValue ? 1 : 0;
            }
        }

        boolean [][] swapRef = map;
        map = newMap;
        newMap = swapRef;

        stepCount += 1;
        delta = alive - oldAlive;

        fireChange();
    }

    private void fireChange() {
        if (handler != null) {
            handler.onUpdated();
        }
    }

    public void clear() {
        for (int i = 0; i <= width; i++) {
            for (int j = 0; j <= height; j++) {
                map[i][j] = false;
            }
        }

        stepCount = 0;
        alive = 0;

        fireChange();
    }


    public void random() {
        Random random = new Random();
        alive = 0;
        stepCount = 0;

        for (int i = 0; i <= width; i++) {
            for (int j = 0; j <= height; j++) {

                boolean value = random.nextBoolean();
                map[i][j] = value;
                alive += value ? 1 : 0;
            }
        }

        fireChange();
    }

    public void setValue(int x, int y, boolean value) {
        if (isValidPosition(x, y)) {
            if (map[x][y] != value) {
                map[x][y] = value;
                alive += value ? 1 : -1;
                fireChange();
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isValidPosition(int x, int y) {
        return (x >= 0 && x <= width && y >= 0 && y <= height);
    }

    public boolean getValue(int x, int y) {
        return isValidPosition(x,y) ? map[x][y] : false;
    }

    public int getStepCount() {
        return stepCount;
    }

    public int getAlive() {
        return alive;
    }

    public int getDelta() {
        return delta;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public boolean [][] getMap() {
        return map.clone();
    }
}