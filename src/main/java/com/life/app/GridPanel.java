package com.life.app;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class GridPanel extends JPanel {

    private JScrollBar verticalScrollBar;
    private JScrollBar horizontalScrollBar;
    private JPanel gridPanel;
    private GridController controller;
    private static final Color lifeColor = new Color(55, 170, 77);

    public GridPanel() {
        gridPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                paintGridComponent(g);
            }
        };

        verticalScrollBar = new JScrollBar();
        horizontalScrollBar = new JScrollBar();

        layoutComponents();
    }

    public void setController(GridController gridController) {
        controller = gridController;
    }

    public JScrollBar getVerticalScrollBar() {
        return verticalScrollBar;
    }

    public JScrollBar getHorizontalScrollBar() {
        return horizontalScrollBar;
    }

    public JPanel getGridPanel() {
        return gridPanel;
    }

    private void layoutComponents() {
        setLayout(new MigLayout("fill"));
        add(gridPanel, "grow, push");
        add(verticalScrollBar, "grow y, wrap");
        add(horizontalScrollBar,"grow x");
    }

    private void paintGridComponent(Graphics g) {
        if (controller == null) return;

        Graphics2D g2d = (Graphics2D) g;

        Color safeColor = g2d.getColor();

        Rectangle viewPort = controller.getViewPort();

        for (int i = 0; i <= viewPort.width; i ++) {
            for (int j = 0; j <= viewPort.height; j++) {

                GridController.CellState state = controller.getCellValue(i, j);

                Color color;

                switch (state) {
                    case EMPTY :
                        color = Color.WHITE;
                        break;
                    case EXISTS:
                        color = lifeColor;
                        break;
                    default:
                        color = Color.BLACK;
                        break;
                }

                g2d.setColor(color);
                g2d.fillRect(i*Setting.GRID_DOT_SIZE, j*Setting.GRID_DOT_SIZE, Setting.GRID_DOT_SIZE, Setting.GRID_DOT_SIZE);
            }
        }

        g2d.setColor(safeColor);
    }

}
