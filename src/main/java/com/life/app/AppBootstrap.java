package com.life.app;

import java.awt.*;

public class AppBootstrap {

    public static void main( String[] args ) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mainFrame = new MainFrame();
                mainFrame.setVisible(true);
            }
        });
    }
}
