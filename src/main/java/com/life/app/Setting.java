package com.life.app;

public final class Setting {
    public static final int GRID_DOT_SIZE = 10;         // pixel size

    public static final int FIELD_MIN_SIZE = 512;       // cell
    public static final int FIELD_MAX_SIZE = 2048;      // cell

    public static final int GAME_TIMER_MIN = 50;        // ms
    public static final int GAME_TIMER_MAX = 500;       // ms

    private Setting() {
    }
}